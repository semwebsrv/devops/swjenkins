# swjenkins-library


Node builds are best done with a pre-built image that has stripes-cli installed - a Dockerfile like this will do the trick

FROM node:14
ENV NODE_ENV=production
RUN npm install --production \
    && yarn config set @folio:registry https://repository.folio.org/repository/npm-folio/ \
    &&  yarn global add @folio/stripes-cli


