/**
 * build a nextjs app in an nginx container.
 * pipelineParams:
 *   baseDir - directory containing project
 */
def call(Map pipelineParams) {

  def buildUtils = new co.semweb.jenkins.Utils()
  private static final List DEFAULT_BRANCHES_TO_DEPLOY = ['origin/master', 'origin/main', 'origin/uat', 'main', 'uat' ]
  def MAINLINE_BRANCHES=[ 'main' ]

  String appdir = 'nextapp'
  String docker_image_name = pipelineParams?.dockerImageName
  String service_name = pipelineParams?.serviceName
  String deployment_template = pipelineParams?.deploymentTemplate
  String cicd_target_namespace = pipelineParams?.targetNamespace
  boolean is_snapshot = true

  def branches_to_deploy = pipelineParams?.branchesToDeploy ?: DEFAULT_BRANCHES_TO_DEPLOY


  println("buildNextJSService(${pipelineParams})");

  podTemplate(
    containers:[
      containerTemplate(name: 'nodejs',  image:'node:14.15.4',                                                           ttyEnabled:true, command:'cat'),
      containerTemplate(name: 'docker',  image:'docker:18',                                                              ttyEnabled:true, command:'cat'),
      containerTemplate(name: 'kubectl', image:'docker.libsdev.k-int.com/knowledgeintegration/kubectl-container:latest', ttyEnabled:true, command:'cat')
    ],
    volumes: [
      hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock')
    ])
  {
    node(POD_LABEL) {

      stage ('checkout') {
        checkout_details = checkout scm
        // props = readProperties file: './service/gradle.properties'
        // deploy_cfg = null;
        env.GIT_BRANCH=checkout_details.GIT_BRANCH
        env.GIT_COMMIT=checkout_details.GIT_COMMIT

        println("Processing commit ${env.GIT_COMMIT} for branch ${env.GIT_BRANCH}");
        String package_file = "${appdir}/package.json".toString()
        package_props = readJSON file: package_file

        println("package_file : ${package_file} == ${package_props}");

        app_version = package_props.version

        println("Process app version ${app_version}");
        constructed_tag = "${app_version}-${currentBuild.number}-${checkout_details?.GIT_COMMIT?.take(12)}"

        env.TARGET_NAMESPACE=pipelineParams.targetNamespace
        env.DEPLOY_AS=pipelineParams.deployAs
        env.CONSTRUCTED_TAG = constructed_tag
        semantic_version_components = app_version.toString().split('\\.')

        //  A build is a release build if it has been tagged with a version number
        if ( app_version ) {
          if ( app_version.contains('SNAPSHOT') ) {
            is_snapshot = true;
          }
          else {
            // Grab the first 12 digits of the commit hash and use those in the tag name
            is_snapshot = false
          }
        }

        println("after checkout env : ${env}");
      }

      // https://www.jenkins.io/doc/book/pipeline/docker/
      stage('Build Docker Image') {
        container('docker') {
          dir(appdir) {
            docker_image = docker.build(docker_image_name)
          }
        }
      }

      stage('Publish docker image') {
        container('docker') {
          dir(appdir) {
            is_mainline_build=true
            if ( is_mainline_build ) {

              println("Considering build tag : ${constructed_tag} version:${app_version} is_snapshot:${is_snapshot}");

              // Some interesting stuff here https://github.com/jenkinsci/pipeline-examples/pull/83/files
              if ( !is_snapshot ) {
                docker.withRegistry('https://docker.semweb.co','semweb-nexus') {
                  println("Publishing released version with latest tag and semver ${semantic_version_components}");
                  docker_image.push('latest')
                  docker_image.push("v${app_version}".toString())
                  docker_image.push("v${semantic_version_components[0]}.${semantic_version_components[1]}".toString())
                  docker_image.push("v${semantic_version_components[0]}".toString())
                }
                // env.MOD_IMAGE="${docker_image_name}:${app_version}"
                // env.TARGET_NAMESPACE=cicd_target_namespace
                // env.SERVICE_ID="${service_name}-${app_version}"
                // env.MOD_DEPLOY_AS=env.SERVICE_ID.replaceAll('\\.','-').toLowerCase()
                env.DEPLOY_IMAGE="${pipelineParams.dockerImageName}:${app_version}".toString()
              }
              else {
                docker.withRegistry('https://docker.semweb.co','semweb-nexus') {
                  println("Publishing snapshot-latest");
                  docker_image.push('snapshot-latest')
                  docker_image.push(constructed_tag)
                  docker_image.push("${app_version}".toString())
                }
                // env.MOD_IMAGE="${docker_image_name}:${app_version}"
                // env.TARGET_NAMESPACE=cicd_target_namespace
                // env.SERVICE_ID="${service_name}-${app_version}.${BUILD_NUMBER}"
                // env.MOD_DEPLOY_AS=env.SERVICE_ID.replaceAll('\\.','-').toLowerCase();
                env.DEPLOY_IMAGE="${pipelineParams.dockerImageName}:${constructed_tag}".toString()
              }
            }
            else {
              println("Not publishing ${is_snapshot?'SNAPSHOT':'RELEASE'} for v${app_version} on branch ${checkout_details?.GIT_BRANCH}. Merge to one of ${MAINLINE_BRANCHES} for docker image");
            }
          }
        }
      }

      stage('Deploy') {

        if ( ( branches_to_deploy.contains(checkout_details?.GIT_BRANCH) ) &&
             ( pipelineParams.deploymentTemplate != null ) &&
             ( pipelineParams.targetNamespace != null ) &&
             ( pipelineParams.deployAs != null ) ) {

          println("Deploy branch ${checkout_details?.GIT_BRANCH} using image ${env.DEPLOY_IMAGE} as ${pipelineParams.deployAs} to NS ${pipelineParams.targetNamespace}");

          container('kubectl') {
            withCredentials([file(credentialsId: 'local_k8s_sf', variable: 'KUBECONFIG')]) {
              String ymlFile = readFile ( pipelineParams.deploymentTemplate )
              println("Resolve template ${pipelineParams.deploymentTemplate} using env");
              String tmpResolved = new groovy.text.SimpleTemplateEngine().createTemplate( ymlFile ).make( [:] + env.getOverriddenEnvironment() ).toString()
              println("Resolved template: ${tmpResolved}");
              writeFile(file: 'module_deploy.yaml', text: tmpResolved)

              println("Get pods");
              sh "kubectl get po -n $target_namespace"
              println("Apply deployment");
              sh 'kubectl apply -f module_deploy.yaml'

              // Remember that this container is itself a pod, so it sees the same DNS discovery as other modules and pods
              // wait for the service to appear
              if ( pipelineParams.healthActuator != null) {
                sh(script: "curl -s --retry-connrefused --retry 15 --retry-delay 10 ${pipelineParams.healthActuator}", returnStdout: true)
              }

              sh "kubectl get all -n ${target_namespace}"
            }
          }
        }
        else {
          println("Not deploying. Missing build config deploymentTemplate/targetNamespace/deployAs/dockerImageName from ${pipelineParams}");
          println("checkout_details?.GIT_BRANCH: ${checkout_details?.GIT_BRANCH}");
          println("pipelineParams.deploymentTemplate : ${pipelineParams.deploymentTemplate}");
          println("pipelineParams.targetNamespace: ${pipelineParams.targetNamespace}");
          println("pipelineParams.deployAs: ${pipelineParams.deployAs}");
        }
      }


    }

    stage ('Remove old builds') {
      //keep 3 builds per branch
      properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '3']]]);
    }

  }
}

