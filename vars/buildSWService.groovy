/**
 *
 * Build a JIB controlled gradle project, publish the container image and optionally deploy the service in the local k8s env
 *
 */
def call(Map pipelineParams) {

  def buildUtils = new co.semweb.jenkins.Utils()
  def branches_to_deploy = ['origin/master', 'origin/main', 'origin/uat' ]

  println("buildSWService(${pipelineParams})");

  podTemplate(
    containers:[
      containerTemplate(name: 'jdk11',   image:'adoptopenjdk:11-jdk-openj9',                                             ttyEnabled:true, command:'cat'),
      containerTemplate(name: 'kubectl', image:'docker.libsdev.k-int.com/knowledgeintegration/kubectl-container:latest', ttyEnabled:true, command:'cat')
    ],
    volumes: [
      hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
      hostPathVolume(hostPath: '/var/lib/jenkins/.gradledist', mountPath: '/root/.gradle')
    ])
  {
    node(POD_LABEL) {
  
      stage ('checkout') {
        checkout_details = checkout scm
        props = readProperties file: './service/gradle.properties'
        app_version = props.appVersion
        deploy_cfg = null;
        env.GIT_BRANCH=checkout_details.GIT_BRANCH
        env.GIT_COMMIT=checkout_details.GIT_COMMIT

        env.TARGET_NAMESPACE=pipelineParams.targetNamespace
        env.DEPLOY_AS=pipelineParams.deployAs

        env.CONSTRUCTED_TAG = "${props?.appVersion}-${currentBuild.number}-${checkout_details?.GIT_COMMIT?.take(12)}"
        semantic_version_components = app_version.toString().split('\\.')

        env.DEPLOY_IMAGE="${pipelineParams.dockerImageName}:${CONSTRUCTED_TAG}".toString()
  
        //  A build is a release build if it has been tagged with a version number
        if ( app_version.contains('SNAPSHOT') ) {
          is_snapshot = true;
        }
        else {
          // Grab the first 12 digits of the commit hash and use those in the tag name
          is_snapshot = false
        }

        println("Got props: ${props} appVersion:${props.appVersion}/${props['appVersion']}/${semantic_version_components} is_snapshot=${is_snapshot}");
        println("Checkout details ${checkout_details}");
        println("Constructed tag: ${env.CONSTRUCTED_TAG}");
      }
  
      stage ('build') {
        container('jdk11') {
          withCredentials([[$class: 'UsernamePasswordMultiBinding', 
                            credentialsId:'semweb-nexus',
                            usernameVariable: 'USERNAME', 
                            passwordVariable: 'PASSWORD']]) {
            dir('service') {
              sh './gradlew --version'
              sh './gradlew --no-daemon -x integrationTest --console=plain -Djib.to.auth.username=$USERNAME -Djib.to.auth.password=$PASSWORD clean build jib'
              sh 'ls ./build/libs'
            }
          }
        }
      }
  
      stage('Deploy Latest Snapshot') {

        if ( ( branches_to_deploy.contains(checkout_details?.GIT_BRANCH) ) &&
             ( pipelineParams.deploymentTemplate != null ) &&
             ( pipelineParams.targetNamespace != null ) &&
             ( pipelineParams.deployAs != null ) ) {

          println("Deploy branch ${checkout_details?.GIT_BRANCH} using image ${env.DEPLOY_IMAGE} as ${pipelineParams.deployAs} to NS ${pipelineParams.targetNamespace}");

          container('kubectl') {
            withCredentials([file(credentialsId: 'local_k8s_sf', variable: 'KUBECONFIG')]) {
              String ymlFile = readFile ( pipelineParams.deploymentTemplate )
              println("Resolve template ${pipelineParams.deploymentTemplate} using ${env}");
              String tmpResolved = new groovy.text.SimpleTemplateEngine().createTemplate( ymlFile ).make( [:] + env.getOverriddenEnvironment() ).toString()
              println("Resolved template: ${tmpResolved}");
              writeFile(file: 'module_deploy.yaml', text: tmpResolved)
  
              println("Get pods");
              sh "kubectl get po -n $target_namespace"
              println("Apply deployment");
              sh 'kubectl apply -f module_deploy.yaml'
  
              // Remember that this container is itself a pod, so it sees the same DNS discovery as other modules and pods
              // wait for the service to appear
              if ( pipelineParams.healthActuator != null) {
                sh(script: "curl -s --retry-connrefused --retry 15 --retry-delay 10 ${pipelineParams.healthActuator}", returnStdout: true)
              }
              else {
                sh(script: "curl -s --retry-connrefused --retry 15 --retry-delay 10 http://${pipelineParams.deployAs}.${target_namespace}:8080/actuator/health", returnStdout: true)
              }

              sh "kubectl get all -n ${target_namespace}"
            }
          }
        }
        else {
          println("Not deploying. Missing build config deploymentTemplate/targetNamespace/deployAs/dockerImageName from ${pipelineParams}");
          println("checkout_details?.GIT_BRANCH: ${checkout_details?.GIT_BRANCH}");
          println("pipelineParams.deploymentTemplate : ${pipelineParams.deploymentTemplate}");
          println("pipelineParams.targetNamespace: ${pipelineParams.targetNamespace}");
          println("pipelineParams.deployAs: ${pipelineParams.deployAs}");
        }
      }
    }

    stage ('Remove old builds') {
      //keep 3 builds per branch
      properties([[$class: 'BuildDiscarderProperty', strategy: [$class: 'LogRotator', artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '3', numToKeepStr: '3']]]);
    }

  }

}
