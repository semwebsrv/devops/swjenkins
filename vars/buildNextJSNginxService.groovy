/**
 * build a nextjs app in an nginx container.
 * pipelineParams:
 *   baseDir - directory containing project
 */
def call(Map pipelineParams) {

  def buildUtils = new co.semweb.jenkins.Utils()
  def branches_to_deploy = ['origin/master', 'origin/main', 'origin/uat' ]

  println("buildSWService(${pipelineParams})");

  podTemplate(
    containers:[
      containerTemplate(name: 'nodejs',  image:'node:14.15.4',                                                           ttyEnabled:true, command:'cat'),
      containerTemplate(name: 'kubectl', image:'docker.libsdev.k-int.com/knowledgeintegration/kubectl-container:latest', ttyEnabled:true, command:'cat')
    ],
    volumes: [
    ])
  {
    node(POD_LABEL) {

      stage ('checkout') {
        checkout_details = checkout scm
        // props = readProperties file: './service/gradle.properties'
        app_version = 'wibble' // props.appVersion
        // deploy_cfg = null;
        env.GIT_BRANCH=checkout_details.GIT_BRANCH
        env.GIT_COMMIT=checkout_details.GIT_COMMIT

        // env.TARGET_NAMESPACE=pipelineParams.targetNamespace
        // env.DEPLOY_AS=pipelineParams.deployAs

        // env.CONSTRUCTED_TAG = "${props?.appVersion}-${currentBuild.number}-${checkout_details?.GIT_COMMIT?.take(12)}"
        // semantic_version_components = app_version.toString().split('\\.')

        // env.DEPLOY_IMAGE="${pipelineParams.dockerImageName}:${CONSTRUCTED_TAG}".toString()

        //  A build is a release build if it has been tagged with a version number
        if ( app_version.contains('SNAPSHOT') ) {
          is_snapshot = true;
        }
        else {
          // Grab the first 12 digits of the commit hash and use those in the tag name
          is_snapshot = false
        }

      }

      stage ('build') {
        println("pass");
      }
    }
  }
}

