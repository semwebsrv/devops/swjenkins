#!/usr/bin/env groovy

package co.semweb.jenkins


import jenkins.model.Jenkins
// import com.cloudbees.groovy.cps.NonCPS
import java.text.SimpleDateFormat


/**
 * A build is a release build if it has been tagged with a version number
 */
def boolean isRelease() {
  def gitTag = sh(returnStdout: true, script: 'git tag -l --points-at HEAD').trim()
  if ( gitTag ==~ /^v\d+.*/ ) {
    println("isRelease() thinks ${gitTag} is a release");
    return true
  }
  else {
    println("isRelease() thinks ${gitTag} is not a release");
    return false
  }
}

// get git tag
def gitTag() {
  def tag = sh(returnStdout: true, script: 'git tag -l --points-at HEAD').trim()
  return tag
}

// get git committer/author
def gitAuthor(String commit) {
  def authorName = sh(script: "git log -n 1 ${commit} --format=%aN", returnStdout: true).trim()
  return authorName 
}

// compare git tag with env.version
def boolean tagMatch(String version) {
  def tag = sh(returnStdout: true, script: 'git tag -l --points-at HEAD | tr -d v').trim()
  if (tag == version) {
    return true
  }
  else {
    return false
  }
}
